# A React Universal Blog App  
a React Universal Blog App that will first render markup on the server side to make our content available to search engines. Then, it will let the browser take over in a single page application that is both fast and responsive.  

Taken from here [part 1](https://www.sitepoint.com/building-a-react-universal-blog-app-a-step-by-step-guide/)  [part 2](https://www.sitepoint.com/building-a-react-universal-blog-app-implementing-flux)  

### Makes use of the following technologies and tools:

> * Node.js for package management and server-side rendering  
> * React for UI views  
> * Express for an easy back-end JS server framework  
> * React Router for routing  
> * React Hot Loader for hot loading in development  
> * Flux for data flow  
> * Cosmic JS for content management  


To get working:   
```npm install```  
    

To run:   
```npm start```  

then go to localhost:3000  
